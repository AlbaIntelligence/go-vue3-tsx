module gitlab.com/geospatialweb/go-vue3-tsx

go 1.16

require (
	github.com/georgysavva/scany v0.2.8
	github.com/gorilla/mux v1.8.0
	github.com/jackc/pgx/v4 v4.11.0
	github.com/paulmach/go.geojson v1.4.0
	github.com/spf13/viper v1.7.1
)
