export default [
  {
    id: 'satellite',
    iconId: 'satellite-icon',
    name: 'Satellite',
    isActive: false,
    src: 'assets/satellite.png',
    height: 20,
    width: 20
  },
  {
    id: 'biosphere',
    iconId: 'biosphere-icon',
    name: 'Biosphere',
    isActive: true,
    src: 'assets/biosphere.png',
    height: 15,
    width: 15
  },
  {
    id: 'office',
    iconId: 'office-icon',
    name: 'Office',
    isActive: false,
    src: 'assets/office.png',
    height: 20,
    width: 18
  },
  {
    id: 'places',
    iconId: 'places-icon',
    name: 'Places',
    isActive: false,
    src: 'assets/places.png',
    height: 20,
    width: 18
  },
  {
    id: 'trails',
    iconId: 'trails-icon',
    name: 'Trails',
    isActive: false,
    src: 'assets/trails.png',
    height: 20,
    width: 18
  },
  {
    id: 'deckgl',
    iconId: 'deckgl-icon',
    name: 'Deck.GL',
    isActive: false,
    src: 'assets/deckgl.png',
    height: 18,
    width: 18
  }
]
